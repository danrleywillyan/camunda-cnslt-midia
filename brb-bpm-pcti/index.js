const jsdom = require('jsdom');

const {JSDOM} = jsdom;
// const {document} = (new JSDOM('<!doctype html><html><body></body></html>')).window;  
// global.document = document;  
// global.window = document.defaultView;

const optionsJSDOM = {
  contentType: "text/html",
  storageQuota: 10000000 //more than this will throw DOMException
};

const htmlFromDODTemplate = `<html><head><style>body{background:white;font-family:Verdana,Calibri,Geneva,Tahoma,sans-serif}.artefato-table{}.artefato-table th{background-color:#00f;color:antiquewhite}.artefato-table td:first-child{background-color:grey;color:black}.artefato-table td:nth-child(2){text-align:justify !important}.table{width:90vw}.table-border{width:90vw;border:1px solid black;border-collapse:collapse}.table-border th, .table-border td{border:1px solid black}.table-border thead{background-color:#c0bfbf}page{background:white;display:block;margin:0 auto;margin-bottom:0.5cm;}@media print{pre,blockquote{page-break-inside:avoid}body,page{margin:0;box-shadow:0;page-break-inside:avoid}div,p,table,html{-webkit-region-break-inside:auto}table,p{page-break-inside:auto}tr{page-break-inside:auto;page-break-after:auto}thead{display:table-header-group}tfoot{display:table-footer-group}.artefato-table{page-break-after:always}}</style><title>PCTI DOD</title><meta charset="UTF-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="author" content="BRB"/><meta name="subject" content="PCTI DOD"/></head><body><table class="artefato-table"><tbody><th style="width: 33%;"> Artefato</th><th style="width: 66%;">Definições</th><tr><td style="width: 30%; text-align: center;"><strong> DOCUMENTO DE OFICIALIZAÇÃO DA DEMANDA (DOD) </strong></td><td style="width: 60%; text-align: center;"><div> <strong>Objetivo:</strong> formalizar o início do processo de planejamento da contratação de TI; vincular as necessidades da contratação aos objetivos estratégicos e às necessidades corporativas da Instituição, todas elas alinhadas ao Plano Diretor de Tecnologia da Informação – PDTI; indicar a fonte de recursos para a contratação; indicar os integrantes da Equipe de Planejamento da Contratação; indicar os parâmetros de classificação de priorização. Deverão ser indicados:<ol type="a"><li> necessidade da contratação, considerando os objetivos estratégicos e as necessidades corporativas, bem como o seu alinhamento ao PDTI;</li><li> explicitação da motivação e demonstrativo de resultados a serem alcançados com a contratação da Solução de Tecnologia da Informação;</li><li> indicação da fonte dos recursos para a contratação;</li><li> indicação da equipe de Planejamento da contratação, e;</li><li> indicação dos critérios para priorização.</li></ol></div><div> <strong>Construção:</strong> Área Requisitante da Solução de TI com apoio do NUPEC – Núcleo de Planejamento e Controle de TI.</div><div> <strong>Participação:</strong> Área Competente da Área de TI e da Área Administrativa.</div><div> <strong>Conteúdo Mínimo:</strong><ol type="1"><li> Identificação da proposição de contratação;</li><li> Identificação da área requisitante da solução;</li><li> Equipe de planejamento da contratação;</li><li> Indicação orçamentária;</li><li> Objetivo estratégico;</li><li> Motivação/Justificativa, e;</li><li> Resultados esperados.</li></ol></div><div> <strong>Encaminhamento:</strong> a tramitação da versão digital será intermediada pelo NUPEC. A tramitação impressa, após conformidade e assinatura devida da equipe de planejamento será feita por meio do envio do DOD pela área requisitante ao Diretor Ditec para avaliação técnica.</div></td></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 1. IDENTIFICAÇÃO DA DEMANDA</th></tr><tr><td> <strong>Nome do Projeto/Solução</strong>: &emsp; {{__brbNomeProjeto}}</td></tr></tbody></table> </br><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 2. IDENTIFICAÇÃO DA ÁREA REQUISITANTE</th></tr><tr><td> <strong>Diretoria/Superintendência/Gerência/Núcleo</strong>: &emsp; {{__brbIdentificacaoAR}}</td></tr><tr><td> <strong>Responsável pela demanda</strong>: &emsp;&emsp;&emsp;&emsp; {{__brbResponsavelDemanda}}</td></tr><tr><td> <strong>Matrícula</strong>: &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; {{__brbMatrículaAR}}</td></tr><tr><td> <strong>E-mail do responsável</strong>: &emsp;&emsp;&emsp;&emsp; {{__brbResponsavelEmail}}</td></tr><tr><td> <strong>Telefone</strong>: &emsp;&emsp;&emsp;&emsp; {{__brbTelefoneAR}}</td></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 3. INDICAÇÃO ORÇAMENTÁRIA</th></tr><tr><table class="table-border"><thead><th style="width: 40%;">Faixa Contábil</th><th style="width: 20%;">Tipo</th><th style="width: 40%;">Valor Estimado</th></thead><tbody id="indicacao-orcamentaria"></tbody></table></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 4. ALINHAMENTO ESTRATÉGICO</th></tr><tr><td> {{__brbAlinhamentoEstrategico}}</td></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 5. MOTIVAÇÃO/JUSTIFICATIVA</th></tr><tr><table class="table-border"><thead></thead><tbody><tr><td> 5.1 - CRITICIDADE - ( {{__brbCriticidade}} )</td></tr><tr><td> 5.2 - SERVIÇOS CONTINUADOS - ( {{__brbServicoContinuado?}} )</td></tr><tr><td> 5.3 - META - ( {{__brbMeta}} )</td></tr><tr><td> 5.4 - FUNDAMENTAÇÃO - ( {{__brbFundamentacao}} )</td></tr></tbody></table></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 6. RESULTADOS A SEREM ALCANÇADOS</th></tr><tr><td> {{__brbResultadosAlcancados}}</td></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 7. EQUIPE DE PLANEJAMENTO</th></tr><tr><table class="table-border"><tbody><tr><td style="width: 30%; background-color: #c0bfbf;">Integrante Requisitante (IR)</td><td style="width: 20%;"></td><td style="width: 30%; background-color: #c0bfbf;">Matrícula</td><td style="width: 20%;"></td></tr><tr><td style="width: 30%; background-color: #c0bfbf;">E-mail do IR</td><td style="width: 20%;"></td><td style="width: 30%; background-color: #c0bfbf;">Telefone do IR</td><td style="width: 20%;"></td></tr><tr><td style="width: 30%; background-color: #c0bfbf;">Integrante Administrativo (IA)</td><td style="width: 20%;"></td><td style="width: 30%; background-color: #c0bfbf;">Matrícula</td><td style="width: 20%;"></td></tr><tr><td style="width: 30%; background-color: #c0bfbf;">E-mail do IA</td><td style="width: 20%;"></td><td style="width: 30%; background-color: #c0bfbf;">Telefone do IA</td><td style="width: 20%;"></td></tr><tr><td style="width: 30%; background-color: #c0bfbf;">Integrante Técnico (IT)</td><td style="width: 20%;"></td><td style="width: 30%; background-color: #c0bfbf;">Matrícula</td><td style="width: 20%;"></td></tr><tr><td style="width: 30%; background-color: #c0bfbf;">E-mail do IT</td><td style="width: 20%;"></td><td style="width: 30%; background-color: #c0bfbf;">Telefone do IT</td><td style="width: 20%;"></td></tr></tbody></table></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 8. ENCAMINHAMENTO</th></tr><tr><td> Em conformidade com o RLC – BRB, Manual de Contratações e Gestão de Contratos e Manual de Contratações de Soluções de TI, encaminha-se à Diretoria de Tecnologia da Informação (DITEC) para decidir motivadamente sobre o prosseguimento do planejamento da contratação.</td></tr><tr><td style="width: 578px;"> </br><p style="text-align: center;">__________________________________</p><p style="text-align: center;">Requisitante da Solução de TI</p><p style="text-align: center;">{{__brbNomeRequisitante}}</p><p style="text-align: center;"><strong>Data</strong>: {{__brbDataRequisitante}}</p></td></tr><tr><td> <strong>De acordo</strong></td></tr><tr><td style="width: 578px;"> </br><p style="text-align: center;">__________________________________</p><p style="text-align: center;">{{__brbNomeSuperintendenteRequisitante}}</p><p style="text-align: center;"><strong>Data</strong>: {{__brbDataSuperintendenteRequisitante}}</p></td></tr><tr><td style="width: 578px;"> </br><p style="text-align: center;">__________________________________</p><p style="text-align: center;">{{__brbNomeDiretorRequisitante}}</p><p style="text-align: center;"><strong>Data</strong>: {{__brbDataDiretorRequisitante}}</p></td></tr></tbody></table> <br/><table class="table"><tbody><tr><th style="text-align: center; color: black; background-color: grey;"> 9. AVALIAÇÃO DO DIRETOR DITEC</th></tr><tr><table class="table-border"><thead></thead><tbody><tr><td style="background-color: #c0bfbf;"> 9.1 Aprovação para o prosseguimento do planejamento da contratação:</td></tr><tr style="height: 100px;"><td> {{__brbDITECdeAcordo}}</td></tr><tr><td style="background-color: #c0bfbf;"> 9.2 Indicar se o planejamento da contratação deverá ou não ser conduzida por um Gerente de Projetos :</td></tr><tr style="height: 100px;"><td> {{__brbDITECacompanharProjeto?}}</td></tr><tr><td style="background-color: #c0bfbf;"> 9.3 Assinatura:</td></tr><tr><td style="width: 578px;"> </br><p style="text-align: center;">__________________________________</p><p style="text-align: center;">{{__brbNomeDiretorDitec}}</p><p style="text-align: center;"><strong>Data</strong>: {{__brbDataDiretorDitec}}</p></td></tr></tbody></table></tr></tbody></table> <br/></body></html>`;

class IndicacaoOrcamentariaTable {
  faixaContabil;
  tipo;
  valorEstimado;
}

const indicacaoOrcamentariaTable = [
  {
    "faixaContabil": "20000-30000",
    "tipo": "1",
    "valorEstimado": "25000"
  },
  {
    "faixaContabil": "22000-33000",
    "tipo": "3",
    "valorEstimado": "12000"
  },
  {
    "faixaContabil": "220000-320000",
    "tipo": "2",
    "valorEstimado": "250000"
  }
];

var hojeFormatado = '01/12/2020';
var __brbNomeProjeto = "Nome8765423";
var __brbIdentificacaoAR = "alguma coisa aaqui";
var __brbResponsavelDemanda = "Danrley";
var __brbMatrículaAR = "1532960";
var __brbResponsavelEmail = "hello@brb.com.br";
var __brbTelefoneAR = "61095648777";
var __brbAlinhamentoEstrategico = "Sim";
var __brbCriticidade = "Baixa";
var __brbServicoContinuado = "Sim";
var __brbMeta = "Sim";
var __brbFundamentacao = `<p>O Sol (do latim sol, solis[12]) é a estrela central do Sistema Solar. Todos os outros corpos do Sistema Solar, como planetas, planetas anões, asteroides, cometas e poeira, bem como todos os satélites associados a estes corpos, giram ao seu redor. Responsável por 99,86% da massa do Sistema Solar, o Sol possui uma massa 332 900 vezes maior que a da Terra, e um volume 1 300 000 vezes maior que o do nosso planeta.[13] A distância da Terra ao Sol é cerca de 150 milhões de quilômetros ou 1 unidade astronômica (UA). Esta distância varia com o ano de um mínimo de 147,1 milhões de quilômetros (0,9833 UA) no perélio (ou periélio) a um máximo de 152,1 milhões de quilômetros (1,017 UA) no afélio, em torno de 4 de julho.[14] A luz solar demora aproximadamente 8 minutos e 18 segundos para chegar à Terra. Energia do Sol na forma de luz solar é armazenada em glicose por organismos vivos através da fotossíntese, processo do qual, direta ou indiretamente, dependem todos os seres vivos que habitam nosso planeta.[15] A energia solar também é responsável pelos fenômenos meteorológicos e o clima na Terra.[16]</p>

<p>É composto primariamente de hidrogênio (74% de sua massa, ou 92% de seu volume) e hélio (24% da massa solar, 7% do volume solar), com traços de outros elementos, incluindo ferro, níquel, oxigênio, silício, enxofre, magnésio, néon, cálcio e crômio.[17] Possui a classe espectral de G2V: G2 indica que a estrela possui uma temperatura de superfície de aproximadamente 5 780 K, o que lhe confere uma cor branca (apesar de ser visto como amarelo, alaranjado ou avermelhado no céu terrestre quando está próximo ao horizonte, o que se deve à dispersão dos raios na atmosfera);[18] O V (5 em números romanos) na classe espectral indica que o Sol, como a maioria das estrelas, faz parte da sequência principal. Isto significa que o astro gera sua energia através da fusão de núcleos de hidrogênio para a formação de hélio. Existem mais de 100 milhões de estrelas da classe G2 na Via Láctea. Considerado anteriormente uma estrela pequena, acredita-se atualmente que o Sol seja mais brilhante do que 85% das estrelas da Via Láctea, sendo a maioria dessas anãs vermelhas.[19][20] O espectro do Sol contém linhas espectrais de metais ionizados e neutros, bem como linhas de hidrogênio muito fracas. A coroa solar expande-se continuamente no espaço, criando o vento solar, uma corrente de partículas carregadas que estende-se até a heliopausa, a cerca de 100 UA do Sol. A bolha no meio interestelar formada pelo vento solar, a heliosfera, é a maior estrutura contínua do Sistema Solar.[21][22]</p>

<p>O Sol orbita em torno do centro da Via Láctea, atravessando no momento a Nuvem Interestelar Local de gás de alta temperatura, no interior do Braço de Órion da Via Láctea, entre os braços maiores Perseus e Sagitário. Das 50 estrelas mais próximas do Sistema Solar, num raio de até 17 anos-luz da Terra, o Sol é a quarta maior em massa.[23] Diferentes valores de magnitude absoluta foram dados para o Sol, como, por exemplo, 4,85,[24] e 4,81.[25] O Sol orbita o centro da Via Láctea a uma distância de cerca de 24 a 26 mil anos-luz do centro galáctico, movendo-se geralmente na direção de Cygnus e completando uma órbita entre 225 a 250 milhões de anos (um ano galáctico). A estimativa mais recente e precisa da velocidade orbital do sol é da ordem de 251 km/s.[26][27] Visto que a Via Láctea move-se na direção da constelação Hidra, com uma velocidade de 550 km/s, a velocidade do Sol relativa à radiação cósmica de fundo em micro-ondas é de 370 km/s, na direção da constelação da Taça.[28]</p>`;
var __brbResultadosAlcancados = `<h1><img alt="" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg/305px-The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg" style="height:291px; width:305px" />Hello world!</h1>

<p>&nbsp;</p>

<p>https://nightly.ckeditor.com/20-10-21-15-04/full/samples/I&#39;m an instance of <a href="https://ckeditor.com">CKEditor</a>.</p>

<h2>&nbsp;Essential things to think about before starting a blog</h2>

<p><img alt="A picture of wooden spoons with spices and herbs in them." src="/assets/images/bg/spoons-3e11659237.jpg" style="width:450px" />​</p>

<p>It takes several ingredients to create a delicious blog.</p>

<p>It has been exactly 3 years since I wrote my first blog series entitled &ldquo;Flavorful Tuscany&rdquo;, but starting it was definitely not easy. Back then, I didn&rsquo;t know much about blogging, let alone think that one day it could become <strong>my full-time job</strong>. Even though I had many recipes and food-related stories to tell, it never crossed my mind that I could be sharing them with the whole world.</p>

<p>I am now a <strong>full-time blogger</strong> and the curator of the <a href="https://ckeditor.com/ckeditor-4/#">Simply delicious newsletter</a>, sharing stories about traveling and cooking, as well as tips on how to run a successful blog.</p>

<p>If you are tempted by the idea of creating your own blog, please think about the following:</p>

<ul>
	<li>Your story (what do you want to tell your audience)</li>
	<li>Your audience (who do you write for)</li>
	<li>Your blog name and design</li>
</ul>

<p>After you get your head around these 3 essentials, all you have to do is grab your keyboard and the rest will follow.</p>
`;

const dom = new JSDOM(htmlFromDODTemplate, optionsJSDOM);

var __brbNomeRequisitante = "Rodney Lataria";
var __brbDataRequisitante = "25/08/1966";
var __brbNomeSuperintendenteRequisitante = "Rodney Lataria";
var __brbDataSuperintendenteRequisitante = "";
var __brbNomeDiretorRequisitante = "Rodney Lataria";
var __brbDataDiretorRequisitante = "25/08/1966";
var __brbDITECdeAcordo = "Sim";
var __brbDITECacompanharProjeto = "Sim";
var __brbNomeDiretorDitec = "Rodney Lataria";
var __brbDataDiretorDitec = "25/08/1966";


function string2Bin(str) {
  var result = [];
  for (var i = 0; i < str.length; i++) {
    result.push(str.charCodeAt(i));
  }
  return result;
}

const generatePDF = (res) => {
  var tmplt = res;
  tmplt     = tmplt.replace(/[\n\r]/g, '');

  // brbDescricaoDeliberacaoNE = brbDescricaoDeliberacaoNE.replace(/(\r\n|\n|\r)/gm, "<br/>");

  
  tmplt = tmplt.replace("{{__brbDate}}",                                        hojeFormatado)
                .replace("{{__brbNomeProjeto}}",                                __brbNomeProjeto)
                .replace("{{__brbIdentificacaoAR}}",                            __brbIdentificacaoAR)
                .replace("{{__brbResponsavelDemanda}}",                         __brbResponsavelDemanda)
                .replace("{{__brbMatrículaAR}}",                                __brbMatrículaAR)
                .replace("{{__brbResponsavelEmail}}",                           __brbResponsavelEmail)
                .replace("{{__brbTelefoneAR}}",                                 __brbTelefoneAR)
                .replace("{{__brbAlinhamentoEstrategico}}",                     __brbAlinhamentoEstrategico)
                .replace("{{__brbCriticidade}}",                                __brbCriticidade)
                .replace("{{__brbServicoContinuado?}",                          __brbServicoContinuado)
                .replace("{{__brbMeta}}",                                       __brbMeta)
                .replace("{{__brbFundamentacao}}",                              __brbFundamentacao)
                .replace("{{__brbResultadosAlcancados}}",                       __brbResultadosAlcancados)
                .replace("{{__brbNomeRequisitante}}",                           __brbNomeRequisitante)
                .replace("{{__brbDataRequisitante}}",                           __brbDataRequisitante)
                .replace("{{__brbNomeSuperintendenteRequisitante}}",            __brbNomeSuperintendenteRequisitante)
                .replace("{{__brbDataSuperintendenteRequisitante}}",            __brbDataSuperintendenteRequisitante)
                .replace("{{__brbNomeDiretorRequisitante}}",                    __brbNomeDiretorRequisitante)
                .replace("{{__brbDataDiretorRequisitante}}",                    __brbDataDiretorRequisitante)
                .replace("{{__brbDITECdeAcordo}}",                              __brbDITECdeAcordo)
                .replace("{{__brbDITECacompanharProjeto?}",                     __brbDITECacompanharProjeto)
                .replace("{{__brbNomeDiretorDitec}}",                           __brbNomeDiretorDitec)
                .replace("{{__brbDataDiretorDitec}}",                           __brbDataDiretorDitec);
  console.log(tmplt);
  var jsnHtml = '';
  try {
      var value = string2Bin(tmplt);
      jsnHtml = '{ "value": "'+ dom.window.btoa(value) + '"} ';
      // jsnHtml = '{ "value": " asdfasdfasdfasfas"} ';
  } catch(e) {
      console.log("---------------------------------------------------------------------");
      console.log("ERROR in parese encode UTF8");
      console.log(e);
      console.log("---------------------------------------------------------------------");
  }
  $http.post(Uri.appUri('http://localhost:8080/converter/api/v2/pdf/html2pdf'), jsnHtml, {
    transformRequest: angular.identity,
    headers: {'Content-Type': 'application/json;charset=UTF-8'}
  })
  .then(function(response) {
    try {
        var file = response.data;

        console.log(file);

        // if (document.getElementById('brbLinkDespacho')) {
        //     var linkDespacho =  document.getElementById('brbLinkDespacho');
        //     linkDespacho.remove();
        // }

        // var ahref = document.createElement('a'); 
        // var link = document.createElement('span');
        // link.setAttribute('class', 'glyphicon glyphicon-download-alt');
        // ahref.appendChild(link);
        // ahref.title = "Download despacho";
        // ahref.id = "brbLinkDespacho";
        // ahref.setAttribute('class', 'btn btn-default');

        // ahref.href = file.headDataToTagHtml + file.data;
        // ahref.download = "Despacho.pdf";
        // ahref.type = "application/pdf";
        // ahref.value = "Despacho";
        // document.getElementById('brbPdfDespacho').appendChild(ahref); 

    } catch (e) {
        console.log("---------------------------------------------------------------------");
        console.log("Erro na criacao do link para realizar o donwload");
        console.log(e);
        console.log("---------------------------------------------------------------------");
    } 
    },
    function(response) { // optional
        // failed
        console.log("---------------------------------------------------------------------");
        console.log("Erro no retorno /converter/api/html2pdf");
        console.log(response);
        console.log("---------------------------------------------------------------------");
    }
  );
}

const modifyHTML = () => {

    // const dom = new jsdom.JSDOM();
    // JSDOM.fromFile("./forms/PCTI-DOD-documento-oficializacao-demanda.html", optionsJSDOM).then(dom => {
      console.log(dom.window.document.getElementById('indicacao-orcamentaria').innerHTML); 
      var table = dom.window.document.getElementById('indicacao-orcamentaria');

      for(var i =0; i< indicacaoOrcamentariaTable.length; i++){
        var row = table.insertRow(i);
        row.insertCell(0).innerHTML = indicacaoOrcamentariaTable[i]['faixaContabil'];
        row.insertCell(1).innerHTML = indicacaoOrcamentariaTable[i]['tipo'];
        row.insertCell(2).innerHTML = indicacaoOrcamentariaTable[i]['valorEstimado'];
      }
      console.log(dom.window.document.getElementById('indicacao-orcamentaria').innerHTML); 
      generatePDF(dom.window.document.documentElement.innerHTML);
      // console.log(dom.window.document.documentElement.innerHTML);
      
    // });
  }

modifyHTML();


